<?php

namespace Weblab\Generic\Entity;

use Pckg\Database\Entity;
use Weblab\Generic\Record\Action;

/**
 * Class Actions
 * @package Weblab\Generic\Entity
 */
class Actions extends Entity
{

    /**
     * @var
     */
    protected $record = Action::class;

    public function layouts()
    {
        return $this->morphedBy(Layouts::class)
            ->over(ActionsMorphs::class)// middle entity
            ->on('action_id')// id of morphs
            ->poly('poly_id')// id of this object
            ->morph('morph_id'); // related class
    }

    /**
     * @return mixed
     */
    public function contents()
    {
        return $this->morphsMany(Contents::class)
            ->over(ActionsMorphs::class)
            ->on('action_id')
            ->poly('poly_id')
            ->morph('morph_id');
    }

}


