<?php

namespace Weblab\Generic\Entity;

use Pckg\Database\Entity;
use Weblab\Generic\Record\Variable;

class Variables extends Entity {

    protected $record = Variable::class;

}