<?php

namespace Weblab\Generic\Service\Generic;

use Exception;
use Pckg\Concept\Reflect;
use Weblab\Generic\Record\Action as ActionRecord;
use Weblab\Generic\Record\ActionsLayout;
use Weblab\Generic\Record\ActionsRoute;
use Weblab\Generic\Record\Route;

/**
 * Class Action
 * @package Weblab\Generic\Service\Generic
 */
class Action
{

    /**
     * @var
     */
    protected $class;

    /**
     * @var
     */
    protected $method;

    /**
     * @var array
     */
    protected $args = [];

    /**
     * @var null
     */
    protected $order;

    /**
     * @param      $class
     * @param      $method
     * @param null $order
     */
    public function __construct($class, $method, $args = [], $order = null)
    {
        $this->class = $class;
        $this->method = $method;
        $this->order = $order;
        $this->args = $args;
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function getClass()
    {
        return $this->class;
    }

    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getHtml()
    {
        if ($this->class && $this->method) {
            $prefix = strtolower(request()->getMethod());

            $args = array_merge($this->args, ['action' => $this]);
            $controller = Reflect::create($this->class, $args);
            $method = ($prefix ? $prefix . ucfirst($this->method) : $this->method);

            try {
                $result = (string)Reflect::method($controller, $method, $args);
            } catch (\Exception $e) {
                if (env()->isDev()) {
                    throw $e;
                }

                // @T00D00 - log error!
                return;
            }

            return '<!-- ' . $this->class . '::' . $method . ' start -->' . $result . '<!-- ' . $this->class . '::' . $method . ' end -->';
        }
    }

}