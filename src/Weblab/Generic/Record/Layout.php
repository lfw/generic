<?php

namespace Weblab\Generic\Record;

use Pckg\Database\Record;
use Weblab\Generic\Entity\Layouts;

class Layout extends Record {

    protected $entity = Layouts::class;

}