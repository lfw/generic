<?php

namespace Weblab\Generic\Record;

use Pckg\Database\Record;
use Weblab\Generic\Entity\Actions;

/**
 * Class Action
 * @package Weblab\Generic\Record
 */
class Action extends Record
{

    /**
     * @var
     */
    protected $entity = Actions::class;

}