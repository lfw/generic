<?php

namespace Weblab\Generic\Record;

use Pckg\Database\Record;
use Weblab\Generic\Entity\Variables;

class Variable extends Record {

    protected $entity = Variables::class;

}