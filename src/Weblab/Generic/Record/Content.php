<?php

namespace Weblab\Generic\Record;

use Pckg\Database\Record;
use Weblab\Generic\Entity\Contents;

/**
 * Class Content
 * @package Weblab\Generic\Record
 */
class Content extends Record
{

    /**
     * @var
     */
    protected $entity = Contents::class;

}